let this_uuid = '';

export const hasDB = jest.fn(() => (this_uuid === 'exists' ? true : false));
export const createDB = jest.fn();
export const create = jest.fn();
export const bluzelle = jest.fn(({ uuid }) => {
  this_uuid = uuid;
  return {
    hasDB,
    createDB,
    create
  };
});
