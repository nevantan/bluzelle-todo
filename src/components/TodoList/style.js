export default ({ common, spacing }) => ({
  ...common,
  card: {
    ...common.card,
    width: '100%'
  },
  list: {
    display: 'flex',
    flexDirection: 'column',
    listStyle: 'none',
    flex: 1,
    margin: `0 0 ${spacing.s} 0`,
    padding: 0,
    width: '100%'
  }
});
