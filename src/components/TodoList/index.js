/* @flow */

// Libraries
import React from 'react';
import injectSheet from 'react-jss';

// Styles
import styles from './style';

// Components
import AddForm from './components/AddForm';
import TodoItem from './components/TodoItem';

// Types
import type { TodoItemType } from './components/TodoItem';
export type Props = {
  id: string,
  bz: Object,
  classes: Object
};
export type State = {
  todos: Array<TodoItemType>
};

export class TodoList extends React.Component<Props, State> {
  static defaultProps = {
    id: '',
    bz: {
      read: () => '[]'
    },
    classes: {}
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      todos: []
    };
  }

  async componentDidMount() {
    const { id, bz } = this.props;
    this.setState({
      todos: JSON.parse(await bz.read(id))
    });
  }

  persistData = async () => {
    const { id, bz } = this.props;
    await bz.update(id, JSON.stringify(this.state.todos));
  };

  finishItem = (id: string) => {
    this.setState(
      (prevState) => ({
        todos: prevState.todos.map((todo) =>
          todo.id === id
            ? {
                ...todo,
                done: !todo.done
              }
            : todo
        )
      }),
      this.persistData
    );
  };

  removeItem = (id: string) => {
    this.setState(
      (prevState) => ({
        todos: prevState.todos.filter((todo) => todo.id !== id || !todo.done)
      }),
      this.persistData
    );
  };

  addItem = (label: string) => {
    this.setState(
      (prevState) => ({
        todos: [
          ...prevState.todos,
          {
            id: label.toLowerCase().replace(' ', '-'),
            label: label,
            done: false
          }
        ]
      }),
      this.persistData
    );
  };

  editItem = (id: string, label: string) => {
    this.setState((prevState) => ({
      todos: prevState.todos.map((todo) =>
        todo.id !== id
          ? todo
          : {
              ...todo,
              label
            }
      )
    }));
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.card}>
        <h1>Todo List</h1>
        <ul className={classes.list}>
          {this.state.todos.map((item, i) => (
            <TodoItem
              {...item}
              className={classes.item}
              finishItem={this.finishItem}
              removeItem={this.removeItem}
              editItem={this.editItem}
              saveItem={this.persistData}
              key={i}
            />
          ))}
        </ul>
        <AddForm addItem={this.addItem} />
      </div>
    );
  }
}

export default injectSheet(styles)(TodoList);
