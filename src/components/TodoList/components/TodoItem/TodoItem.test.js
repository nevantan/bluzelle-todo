import React from 'react';
import { shallow } from 'enzyme';

import { TodoItem } from './';

describe('the TodoItem component', () => {
  it('should render correctly by default', () => {
    const wrapper = shallow(<TodoItem label="Test Item" />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render a complete button when unfinished', () => {
    const wrapper = shallow(
      <TodoItem label="Test Item" classes={{ finish: 'finish' }} done={false} />
    );
    expect(wrapper.exists('.finish')).toBe(true);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render an unfinish button and a delete button when done', () => {
    const wrapper = shallow(
      <TodoItem
        label="Test Item"
        classes={{ delete: 'delete', unfinish: 'unfinish' }}
        done={true}
      />
    );
    expect(wrapper.exists('.delete')).toBe(true);
    expect(wrapper.exists('.unfinish')).toBe(true);
    expect(wrapper).toMatchSnapshot();
  });

  it('should finish an item when button clicked', () => {
    const id = 'testitem';
    const finishItem = jest.fn();
    const removeItem = jest.fn();

    const wrapper = shallow(
      <TodoItem
        id={id}
        label="Test Item"
        classes={{ delete: 'delete', finish: 'finish' }}
        done={false}
        finishItem={finishItem}
        removeItem={removeItem}
      />
    );
    wrapper.find('.finish').simulate('click', {
      stopPropagation: () => {}
    });
    expect(finishItem).toHaveBeenCalledTimes(1);
    expect(finishItem).toHaveBeenCalledWith(id);
    expect(removeItem).toHaveBeenCalledTimes(0);
  });

  it('should delete a finished item when button clicked', () => {
    const id = 'testitem';
    const finishItem = jest.fn();
    const removeItem = jest.fn();

    const wrapper = shallow(
      <TodoItem
        id={id}
        label="Test Item"
        classes={{ delete: 'delete', finish: 'finish' }}
        done={true}
        finishItem={finishItem}
        removeItem={removeItem}
      />
    );
    wrapper.find('.delete').simulate('click', {
      stopPropagation: () => {}
    });
    expect(finishItem).toHaveBeenCalledTimes(0);
    expect(removeItem).toHaveBeenCalledTimes(1);
    expect(removeItem).toHaveBeenCalledWith(id);
  });

  it('should edit label and save onBlur', () => {
    const id = 'testitem';
    const value = 'Foo';
    const editItem = jest.fn();
    const saveItem = jest.fn();

    const wrapper = shallow(
      <TodoItem
        id={id}
        label="Test Item"
        classes={{ item: 'item', editField: 'editfield' }}
        done={false}
        editItem={editItem}
        saveItem={saveItem}
      />
    );

    wrapper.find('.item').simulate('click', {
      stopPropagation: () => {}
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(1);

    wrapper.find('.editfield').simulate('change', {
      target: {
        value
      }
    });
    expect(editItem).toHaveBeenCalledTimes(1);
    expect(editItem).toHaveBeenCalledWith(id, value);

    wrapper.find('.editfield').simulate('blur', {
      stopPropagation: () => {}
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(0);
    expect(saveItem).toHaveBeenCalledTimes(1);
  });

  it('should edit label and save onClick button', () => {
    const id = 'testitem';
    const value = 'Foo';
    const editItem = jest.fn();
    const saveItem = jest.fn();

    const wrapper = shallow(
      <TodoItem
        id={id}
        label="Test Item"
        classes={{ item: 'item', editField: 'editfield' }}
        done={false}
        editItem={editItem}
        saveItem={saveItem}
      />
    );

    wrapper.find('.item').simulate('click', {
      stopPropagation: () => {}
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(1);

    wrapper.find('.editfield').simulate('change', {
      target: {
        value
      }
    });
    expect(editItem).toHaveBeenCalledTimes(1);
    expect(editItem).toHaveBeenCalledWith(id, value);

    wrapper.find('.save').simulate('click', {
      stopPropagation: () => {}
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(0);
    expect(saveItem).toHaveBeenCalledTimes(1);
  });

  it('should edit label and save onKeyDown enter', () => {
    const id = 'testitem';
    const value = 'Foo';
    const editItem = jest.fn();
    const saveItem = jest.fn();

    const wrapper = shallow(
      <TodoItem
        id={id}
        label="Test Item"
        classes={{ item: 'item', editField: 'editfield' }}
        done={false}
        editItem={editItem}
        saveItem={saveItem}
      />
    );

    wrapper.find('.item').simulate('click', {
      stopPropagation: () => {}
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(1);

    wrapper.find('.editfield').simulate('change', {
      target: {
        value
      }
    });
    expect(editItem).toHaveBeenCalledTimes(1);
    expect(editItem).toHaveBeenCalledWith(id, value);

    wrapper.find('.editfield').simulate('keydown', {
      stopPropagation: () => {},
      key: 'Enter'
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(0);
    expect(saveItem).toHaveBeenCalledTimes(1);
  });

  it('should edit label and not save onKeyDown not enter', () => {
    const id = 'testitem';
    const value = 'Foo';
    const editItem = jest.fn();
    const saveItem = jest.fn();

    const wrapper = shallow(
      <TodoItem
        id={id}
        label="Test Item"
        classes={{ item: 'item', editField: 'editfield' }}
        done={false}
        editItem={editItem}
        saveItem={saveItem}
      />
    );

    wrapper.find('.item').simulate('click', {
      stopPropagation: () => {}
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(1);

    wrapper.find('.editfield').simulate('change', {
      target: {
        value
      }
    });
    expect(editItem).toHaveBeenCalledTimes(1);
    expect(editItem).toHaveBeenCalledWith(id, value);

    wrapper.find('.editfield').simulate('keydown', {
      stopPropagation: () => {},
      key: 'D'
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(1);
    expect(saveItem).toHaveBeenCalledTimes(0);
  });

  it('should cancel the edit onKeyDown escape', () => {
    const id = 'testitem';
    const label = 'Test Item';
    const value = 'Foo';
    const editItem = jest.fn();
    const saveItem = jest.fn();

    const wrapper = shallow(
      <TodoItem
        id={id}
        label={label}
        classes={{ item: 'item', editField: 'editfield' }}
        done={false}
        editItem={editItem}
        saveItem={saveItem}
      />
    );

    wrapper.find('.item').simulate('click', {
      stopPropagation: () => {}
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(1);

    wrapper.find('.editfield').simulate('change', {
      target: {
        value
      }
    });
    expect(editItem).toHaveBeenCalledTimes(1);
    expect(editItem).toHaveBeenCalledWith(id, value);

    wrapper.find('.editfield').simulate('keydown', {
      stopPropagation: () => {},
      key: 'Escape'
    });
    wrapper.update();
    expect(wrapper.find('.editfield').length).toBe(0);
    expect(saveItem).toHaveBeenCalledTimes(0);
    expect(editItem).toHaveBeenCalledWith(id, label);
  });
});
