export default (theme) => ({
  ...theme.common,
  item: {
    alignItems: 'center',
    cursor: 'pointer',
    display: 'flex',
    fontSize: theme.font.size.xs,
    justifyContent: 'flex-start',

    '& .edit': {
      display: 'none',
      fontSize: '70%',
      marginLeft: theme.spacing.xs
    },

    '& .save': {
      fontSize: '70%',
      marginLeft: theme.spacing.xs
    },

    '&:hover .edit': {
      display: 'block'
    }
  },
  editField: {
    backgroundColor: 'transparent',
    border: 0,
    borderBottom: `2px solid ${theme.color.light}`,
    color: theme.color.light,
    font: 'inherit',
    outline: 0
  },
  done: {
    color: '#999',
    textDecoration: 'line-through'
  },
  delete: {
    color: '#c00',
    fontSize: '70%',
    margin: `${theme.spacing.xxs} 0 0 ${theme.spacing.xs}`
  },
  finish: {
    cursor: 'pointer',
    margin: `0 ${theme.spacing.s} 0 0`
  }
});
