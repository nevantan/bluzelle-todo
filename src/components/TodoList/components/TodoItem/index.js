/* @flow */

// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import Fontawesome from 'react-fontawesome';

// Styles
import styles from './style';

// Types
export type Props = {
  classes: Object,
  id: string,
  label: string,
  done: boolean,
  removeItem: Function,
  finishItem: Function,
  editItem: Function,
  saveItem: Function
};
export type State = {
  editing: boolean,
  lastLabel: string
};
export type TodoItemType = {
  id: string,
  label: string,
  done: boolean
};

export class TodoItem extends React.Component<Props, State> {
  static defaultProps = {
    classes: {},
    done: false
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      editing: false,
      lastLabel: props.label
    };
  }

  saveItem = (e: Object) => {
    e.stopPropagation();
    this.setState(
      {
        editing: false,
        lastLabel: this.props.label
      },
      () => {
        this.props.saveItem();
      }
    );
  };

  cancelEdit = (e: Object) => {
    e.stopPropagation();
    this.setState(
      {
        editing: false
      },
      () => {
        this.props.editItem(this.props.id, this.state.lastLabel);
      }
    );
  };

  render() {
    const {
      classes,
      id,
      label,
      done,
      removeItem,
      finishItem,
      editItem
    } = this.props;

    return (
      <li
        className={`${classes.item} ${done ? classes.done : ''}`}
        onClick={(e) => {
          e.stopPropagation();
          this.setState({ editing: true });
        }}
      >
        <button
          className={`${classes.fa_button} ${classes.finish} ${
            done ? classes.unfinish : ''
          }`}
          onClick={(e) => {
            e.stopPropagation();
            finishItem(id);
          }}
        >
          {done ? (
            <Fontawesome name="check-square" />
          ) : (
            <Fontawesome name="square" />
          )}
        </button>
        {this.state.editing ? (
          <input
            autoFocus
            className={classes.editField}
            type="text"
            value={label}
            onChange={(e) => {
              editItem(id, e.target.value);
            }}
            onBlur={this.saveItem}
            onKeyDown={(e) => {
              if (e.key === 'Enter') this.saveItem(e);
              else if (e.key === 'Escape') this.cancelEdit(e);
            }}
          />
        ) : (
          <span>{label}</span>
        )}
        {this.state.editing ? (
          <button
            className={`${classes.fa_button} save`}
            onClick={this.saveItem}
          >
            <Fontawesome name="check" />
          </button>
        ) : (
          <span className={`${classes.fa_button} edit`}>
            <Fontawesome name="edit" />
          </span>
        )}

        {done && (
          <button
            className={`${classes.fa_button} ${classes.delete}`}
            onClick={(e) => {
              e.stopPropagation();
              removeItem(id);
            }}
          >
            <Fontawesome name="trash-alt" />
          </button>
        )}
      </li>
    );
  }
}

export default injectSheet(styles)(TodoItem);
