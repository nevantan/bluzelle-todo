import React from 'react';
import { shallow } from 'enzyme';

import { AddForm } from './';

describe('the AddForm component', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<AddForm />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should set the state on input', () => {
    const value = 'Sample Todo';

    const wrapper = shallow(<AddForm />);
    wrapper.find('#label').simulate('change', {
      target: {
        value
      }
    });

    expect(wrapper.state('label')).toBe(value);
  });

  it('should add an item to the database', () => {
    const label = 'Sample Todo';
    const addItem = jest.fn();
    const preventDefault = jest.fn();

    const wrapper = shallow(<AddForm addItem={addItem} />);
    wrapper.setState({
      label
    });
    wrapper.find('#add-item-form').simulate('submit', { preventDefault });

    expect(preventDefault).toHaveBeenCalledTimes(1);
    expect(addItem).toHaveBeenCalledTimes(1);
    expect(addItem).toHaveBeenCalledWith(label);
  });

  it('clears the input on submit', () => {
    const wrapper = shallow(<AddForm />);
    wrapper.find('#label').simulate('change', {
      target: {
        value: 'Foo'
      }
    });
    wrapper.update();

    wrapper
      .find('#add-item-form')
      .simulate('submit', { preventDefault: () => {} });
    wrapper.update();

    expect(wrapper.state('label')).toBe('');
  });
});
