/* @flow */

// Libraries
import React from 'react';
import injectSheet from 'react-jss';

// Styles
import styles from './style';

// Types
export type Props = {
  classes: Object,
  addItem: Function
};
export type State = {
  label: string
};

export class AddForm extends React.Component<Props, State> {
  static defaultProps = {
    classes: {},
    addItem: () => {}
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      label: ''
    };
  }

  handleSubmit = (e: Object) => {
    e.preventDefault();

    this.props.addItem(this.state.label);

    this.setState({
      label: ''
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <form
        className={classes.form}
        id="add-item-form"
        onSubmit={this.handleSubmit}
      >
        <input
          type="text"
          id="label"
          value={this.state.label}
          onChange={(e) => {
            this.setState({ label: e.target.value });
          }}
        />

        <button className={classes.button}>Add Item</button>
      </form>
    );
  }
}

export default injectSheet(styles)(AddForm);
