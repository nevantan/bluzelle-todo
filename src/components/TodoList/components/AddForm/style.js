export default (theme) => ({
  ...theme.common,
  form: {
    display: 'flex',

    '& input': {
      flex: 1,
      marginRight: theme.spacing.xs
    },

    '& input, & button': {
      fontFamily: 'inherit',
      fontSize: theme.font.size.xs,
      padding: theme.spacing.xs
    }
  }
});
