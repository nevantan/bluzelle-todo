import React from 'react';
import { shallow } from 'enzyme';

import { TodoList } from './';

describe('the TodoList component', () => {
  const bz = {
    update: () => {},
    read: () =>
      new Promise((resolve) => {
        resolve('[]');
      })
  };

  it('should render correctly', () => {
    const wrapper = shallow(<TodoList />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render all of its todos', () => {
    const wrapper = shallow(<TodoList classes={{ item: 'item' }} />);
    wrapper.setState({
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Bar', done: false }
      ]
    });
    expect(wrapper.find('.item').length).toBe(2);
  });

  it('should complete an unfinished todo', () => {
    const state = {
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Bar', done: false }
      ]
    };
    const nextState = {
      todos: [
        { id: 'foo', label: 'Foo', done: true },
        { id: 'bar', label: 'Bar', done: false }
      ]
    };

    const wrapper = shallow(<TodoList bz={bz} />);
    wrapper.setState(state);
    wrapper.instance().finishItem('foo');
    wrapper.update();

    expect(wrapper.state()).toEqual(nextState);
  });

  it('should uncompleted a finished todo', () => {
    const state = {
      todos: [
        { id: 'foo', label: 'Foo', done: true },
        { id: 'bar', label: 'Bar', done: false }
      ]
    };
    const nextState = {
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Bar', done: false }
      ]
    };

    const wrapper = shallow(<TodoList bz={bz} />);
    wrapper.setState(state);
    wrapper.instance().finishItem('foo');
    wrapper.update();

    expect(wrapper.state()).toEqual(nextState);
  });

  it('should remove a complete todo', () => {
    const state = {
      todos: [
        { id: 'foo', label: 'Foo', done: true },
        { id: 'bar', label: 'Bar', done: false }
      ]
    };
    const nextState = {
      todos: [{ id: 'bar', label: 'Bar', done: false }]
    };

    const wrapper = shallow(<TodoList bz={bz} />);
    wrapper.setState(state);
    wrapper.instance().removeItem('foo');
    wrapper.update();

    expect(wrapper.state()).toEqual(nextState);
  });

  it('should not delete an incomplete todo', () => {
    const state = {
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Bar', done: true }
      ]
    };
    const nextState = {
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Bar', done: true }
      ]
    };

    const wrapper = shallow(<TodoList bz={bz} />);
    wrapper.setState(state);
    wrapper.instance().removeItem('foo');
    wrapper.update();

    expect(wrapper.state()).toEqual(nextState);
  });

  it('should add an item', () => {
    const state = {
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Bar', done: true }
      ]
    };

    const nextState = {
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Bar', done: true },
        { id: 'baz', label: 'Baz', done: false }
      ]
    };

    const wrapper = shallow(<TodoList bz={bz} />);
    wrapper.setState(state);
    wrapper.instance().addItem('Baz');
    expect(wrapper.state()).toEqual(nextState);
  });

  it('should edit an item', () => {
    const state = {
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Baz', done: true }
      ]
    };

    const nextState = {
      todos: [
        { id: 'foo', label: 'Foo', done: false },
        { id: 'bar', label: 'Baz', done: true }
      ]
    };

    const wrapper = shallow(<TodoList bz={bz} />);
    wrapper.setState(state);
    wrapper.instance().editItem('bar', 'Baz');
    expect(wrapper.state()).toEqual(nextState);
  });
});
