/* @flow */

// Libraries
import React from 'react';
import { keys } from 'libp2p-crypto';
import aesjs from 'aes-js';
import sha256 from 'js-sha256';
import injectSheet from 'react-jss';
import uuidv4 from 'uuid/v4';

// Styles
import styles from './style';

// Types
export type Props = {
  classes: Object,
  callback: Function
};
export type Account = {
  id: String,
  label: String,
  key: Buffer
};
export type State = {
  activeAccount: string,
  accounts: Array<Account>,
  label: string,
  password: string
};

export class LoginForm extends React.Component<Props, State> {
  static defaultProps = {
    classes: {}
  };

  constructor(props: Props) {
    super(props);

    const item = localStorage.getItem('accounts');
    const accounts = JSON.parse(item || '[]');

    this.state = {
      activeAccount: accounts.length > 0 ? accounts[0].id : 'new',
      accounts,
      label: '',
      password: ''
    };
  }

  handleSubmit = async (e: Object) => {
    e.preventDefault();

    if (this.state.activeAccount === 'new')
      await this.createAccount(this.state.label, this.state.password);
    else
      await this.login(
        this.state.activeAccount,
        this.state.password,
        this.props.callback
      );
  };

  createAccount = async (label: string, password: string) => {
    const key = await new Promise((resolve, reject) => {
      keys.generateKeyPair('secp256k1', 256, (_, key) => {
        resolve(key);
      });
    });

    // Serialize private key
    const serialized = keys.marshalPrivateKey(key);

    // Derive AES key
    const passBuffer = Buffer.from(sha256(password).substring(0, 32));

    // Encrypt private key
    let aes = new aesjs.ModeOfOperation.ctr(passBuffer);
    const cipherbytes = aes.encrypt(serialized);
    const cipherhex = aesjs.utils.hex.fromBytes(cipherbytes);

    let accounts = localStorage.getItem('accounts');
    if (!accounts) accounts = '[]';
    accounts = JSON.parse(accounts);

    const id = sha256(label);
    if (accounts.some((account) => account.id === id)) {
      throw new Error('Duplicate account label');
    }

    accounts.push({
      id,
      label,
      key: cipherhex,
      uuid: uuidv4()
    });
    localStorage.setItem('accounts', JSON.stringify(accounts));
    this.setState({ activeAccount: id, accounts });
  };

  login = async (
    id: string,
    password: string,
    success: Function,
    failure: Function
  ) => {
    let accounts = JSON.parse(localStorage.getItem('accounts') || '[]');
    if (accounts.length === 0) throw new Error('There are no accounts');

    const account = accounts.filter((account) => account.id === id)[0];

    // Decrypt private key
    const passBuffer = Buffer.from(sha256(password).substring(0, 32));
    const aes = new aesjs.ModeOfOperation.ctr(passBuffer);
    const decodedCipherBytes = aesjs.utils.hex.toBytes(account.key);
    const clearbytes = aes.decrypt(decodedCipherBytes);

    try {
      const key = await new Promise((resolve, reject) => {
        keys.unmarshalPrivateKey(Buffer.from(clearbytes, 'hex'), (err, key) => {
          if (err) reject('Incorrect password');
          resolve(key);
        });
      });

      success(key, account.uuid);
    } catch (e) {
      throw new Error(e);
    }
  };

  render() {
    const { classes } = this.props;

    return (
      <form
        id="loginform"
        className={`${classes.card} ${classes.form}`}
        onSubmit={this.handleSubmit}
      >
        <h1>Login</h1>

        <label htmlFor="account">Select Account</label>
        <select
          id="account"
          value={this.state.activeAccount}
          onChange={(e) => {
            this.setState({ activeAccount: e.target.value });
          }}
        >
          {this.state.accounts.map((account, i) => (
            <option value={account.id} key={i}>
              {account.label}
            </option>
          ))}
          <option value="new">New account...</option>
        </select>

        {this.state.activeAccount === 'new' && (
          <React.Fragment>
            <label htmlFor="label">Account Label</label>
            <input
              type="text"
              name="label"
              id="label"
              value={this.state.label}
              onChange={(e) => {
                this.setState({ label: e.target.value });
              }}
            />
          </React.Fragment>
        )}

        <label htmlFor="password">Password</label>
        <input
          type="password"
          name="password"
          id="password"
          value={this.state.password}
          onChange={(e) => {
            this.setState({ password: e.target.value });
          }}
        />

        <button type="submit" id="login" className={classes.button}>
          {this.state.activeAccount === 'new' ? 'Create' : 'Login'}
        </button>
      </form>
    );
  }
}

export default injectSheet(styles)(LoginForm);
