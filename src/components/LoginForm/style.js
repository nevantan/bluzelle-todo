export default (theme) => ({
  ...theme.common,
  form: {
    color: theme.color.light,
    flex: 1,
    fontSize: theme.font.size.xs,
    margin: `${theme.spacing.l} 0 0 0`,
    maxWidth: '25rem',

    '& label': {
      fontSize: '80%',
      margin: `0 0 ${theme.spacing.xxs} ${theme.spacing.xxs}`
    },
    '& input, & select': {
      backgroundColor: theme.color.light,
      border: 0,
      borderRadius: '2px',
      color: theme.color.dark,
      font: 'inherit',
      margin: `0 0 ${theme.spacing.s} 0`,
      padding: theme.spacing.xs
    }
  }
});
