import React from 'react';
import faker from 'faker';
import { mount, shallow } from 'enzyme';

import { LoginForm } from './';

describe('the LoginForm component', () => {
  const removeAccounts = () => {
    global.localStorage.removeItem('accounts');
  };

  beforeEach(() => {
    global.localStorage.setItem(
      'accounts',
      JSON.stringify([
        { id: 'test_account', label: 'Test Account', key: 'foobar' }
      ])
    );
  });

  afterEach(() => {
    removeAccounts();
  });

  it('should render correctly by default', () => {
    removeAccounts();

    const wrapper = shallow(<LoginForm />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should display accounts from localStorage properly', () => {
    const getItem = jest.spyOn(global.localStorage.__proto__, 'getItem');

    const wrapper = mount(<LoginForm />);
    const account = wrapper.find('#account');
    expect(getItem).toHaveBeenCalledTimes(1);
    expect(account.props().value).toBe('test_account');
  });

  it('should display the Account Label field at the appropriate time only', () => {
    const wrapper = mount(<LoginForm />);
    const account = wrapper.find('#account');
    account.simulate('change', { target: { value: 'new' } });
    wrapper.update();
    expect(wrapper.find('#account').props().value).toBe('new');
    expect(wrapper.find('#label').length).toBe(1);

    account.simulate('change', { target: { value: 'test_account' } });
    wrapper.update();
    expect(wrapper.find('#account').props().value).toBe('test_account');
    expect(wrapper.find('#label').length).toBe(0);
  });

  it('should update state on input', () => {
    const label = faker.lorem.word();
    const password = faker.lorem.word();

    const wrapper = mount(<LoginForm />);
    wrapper.setState({
      activeAccount: 'new'
    });

    wrapper.find('#label').simulate('change', { target: { value: label } });
    expect(wrapper.state('label')).toBe(label);

    wrapper
      .find('#password')
      .simulate('change', { target: { value: password } });
    expect(wrapper.state('password')).toBe(password);
  });

  it('should create a new account in localStorage', async () => {
    removeAccounts();

    const wrapper = mount(<LoginForm />);

    await wrapper.instance().createAccount('Another Account', 'foobarbaz');

    const accountStr = global.localStorage.getItem('accounts');
    const accounts = JSON.parse(accountStr);
    expect(accounts.length).toBe(1);
    expect(accounts[0].id).toBe(
      '3d57b98290301cde75bfa0af8fca89cca82bc955b637dde7d09c3bc345feef4d'
    );
    expect(accounts[0].label).toBe('Another Account');
    expect(accounts[0].key.length).toBe(72);
  });

  it('should reject duplicate account labels', async () => {
    const wrapper = mount(<LoginForm />);

    try {
      await wrapper.instance().createAccount('Test Account', 'foobarbaz');
      await wrapper.instance().createAccount('Test Account', 'anotherpass');
      expect('Function should have thrown').toBe(true);
    } catch (e) {
      expect(e.message).toBe('Duplicate account label');
    }
  });

  it('should login with the correct password', async () => {
    const success = jest.fn();
    const failure = jest.fn();

    const wrapper = mount(<LoginForm />);

    await wrapper.instance().createAccount('Another Account', 'foobarbaz');

    await wrapper
      .instance()
      .login(
        '3d57b98290301cde75bfa0af8fca89cca82bc955b637dde7d09c3bc345feef4d',
        'foobarbaz',
        success
      );

    expect(success).toHaveBeenCalledTimes(1);
  });

  it('should not login with the incorrect password', async () => {
    const success = jest.fn();
    const failure = jest.fn();

    const wrapper = mount(<LoginForm />);

    await wrapper.instance().createAccount('Another Account', 'foobarbaz');

    try {
      await wrapper
        .instance()
        .login(
          '3d57b98290301cde75bfa0af8fca89cca82bc955b637dde7d09c3bc345feef4d',
          'this_is_an_incorrect_password',
          success
        );
      expect('Function to have thrown').toBe(true);
    } catch (e) {
      expect(e.message).toBe('Incorrect password');
    }

    expect(success).toHaveBeenCalledTimes(0);
  });

  it('should not login when there are no accounts', async () => {
    removeAccounts();

    const wrapper = mount(<LoginForm />);

    try {
      await wrapper.instance().login('', '', () => {});
      expect('Function to have thrown').toBe(true);
    } catch (e) {
      expect(e.message).toBe('There are no accounts');
    }
  });

  it('should handle form submit', () => {
    const login = jest.fn();
    const createAccount = jest.fn();
    const preventDefault = jest.fn();

    const wrapper = mount(<LoginForm />);
    wrapper.setState({
      activeAccount: 'test_account',
      password: 'foobarbaz'
    });

    let instance = wrapper.instance();
    instance.login = login;
    instance.handleSubmit({ preventDefault });
    expect(preventDefault).toHaveBeenCalledTimes(1);
    expect(login).toHaveBeenCalledTimes(1);
    expect(createAccount).toHaveBeenCalledTimes(0);

    wrapper.setState({
      activeAccount: 'new',
      label: 'Another Test Account',
      password: 'anotherpass'
    });
    instance = wrapper.instance();
    instance.createAccount = createAccount;
    instance.handleSubmit({ preventDefault });
    expect(preventDefault).toHaveBeenCalledTimes(2);
    expect(login).toHaveBeenCalledTimes(1);
    expect(createAccount).toHaveBeenCalledTimes(1);
  });
});
