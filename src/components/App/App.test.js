import React from 'react';
import { shallow } from 'enzyme';
import { keys } from 'libp2p-crypto';
import uuidv4 from 'uuid/v4';

// Mocks
jest.mock('bluzelle');
import { bluzelle, hasDB, createDB, create } from 'bluzelle';

import { App } from './';

describe('the App component', () => {
  afterEach(() => {
    bluzelle.mockClear();
    hasDB.mockClear();
    createDB.mockClear();
    create.mockClear();
  });

  it('renders correctly', () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toMatchSnapshot();
  });

  it('sets state on login', async (done) => {
    const key = await new Promise((resolve, reject) => {
      keys.generateKeyPair('secp256k1', 256, (_, key) => {
        resolve(key);
      });
    });

    const wrapper = shallow(<App />);
    await wrapper.instance().handleLogin(key, uuidv4());
    wrapper.update();
    expect(wrapper.state('key')).toEqual(key);
    expect(bluzelle).toHaveBeenCalledTimes(1);
    expect(hasDB).toHaveBeenCalledTimes(1);
    expect(createDB).toHaveBeenCalledTimes(1);
    expect(create).toHaveBeenCalledTimes(2);

    done();
  });

  it('ignores an existing uuid', async (done) => {
    const key = await new Promise((resolve, reject) => {
      keys.generateKeyPair('secp256k1', 256, (_, key) => {
        resolve(key);
      });
    });

    const wrapper = shallow(<App />);
    await wrapper.instance().handleLogin(key, 'exists');
    wrapper.update();
    expect(wrapper.state('key')).toEqual(key);
    expect(bluzelle).toHaveBeenCalledTimes(1);
    expect(hasDB).toHaveBeenCalledTimes(1);
    expect(createDB).toHaveBeenCalledTimes(0);
    expect(create).toHaveBeenCalledTimes(0);

    done();
  });
});
