export default (theme) => ({
  '@import': `url('https://fonts.googleapis.com/css?family=Raleway')`,
  '@global': {
    '*': {
      boxSizing: 'border-box'
    },
    'html, body': {
      margin: 0,
      padding: 0
    },
    body: {
      backgroundColor: theme.color.dark,
      color: theme.color.light,
      fontFamily: theme.font.family.raleway,
      fontSize: '62.5%'
    }
  },
  ...theme.common
});
