/* @flow */

// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import { bluzelle } from 'bluzelle';
import KeyEncoder from 'key-encoder';

// Components
import LoginForm from '../LoginForm';
import TodoList from '../TodoList';

// Styles
import styles from './style';

// Types
export type Props = {
  classes: Object
};
export type State = {
  key: Object,
  bz: Object
};

export class App extends React.Component<Props, State> {
  static defaultProps = {
    classes: {}
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      key: {},
      bz: {}
    };
  }

  handleLogin = async (key: Object, uuid: string) => {
    // Encode private key (PEM)
    const encoder = new KeyEncoder('secp256k1');
    const pem = encoder
      .encodePrivate(Buffer.from(key._key, 'hex'), 'raw', 'pem')
      .split('\n')
      .slice(1, -1)
      .join('');

    // Create Bluzelle client instance
    const bz = bluzelle({
      entry: process.env.ENDPOINT,
      uuid,
      private_pem: pem
    });

    // If the UUID does not yet have a database, try to create one
    if (!(await bz.hasDB())) {
      await bz.createDB();
      await bz.create(
        'lists',
        JSON.stringify([{ id: 'my_first_list', title: 'My First List' }])
      );
      await bz.create(
        'my_first_list',
        JSON.stringify([{ id: 'first_task', label: 'First Task', done: false }])
      );
    }

    this.setState({
      key,
      bz
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.wrapper}>
        {this.state.key._key ? (
          <TodoList id="my_first_list" bz={this.state.bz} />
        ) : (
          <LoginForm callback={this.handleLogin} />
        )}
      </div>
    );
  }
}

export default injectSheet(styles)(App);
