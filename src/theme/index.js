const color = {
  dark: '#071013',
  primary: '#10262d',
  secondary: '#cc0095',
  light: '#fff8f0'
};

const font = {
  family: {
    raleway: '"Raleway", sans-serif'
  },
  size: {
    xxs: '1.2rem',
    xs: '1.4rem',
    s: '1.8rem',
    m: '2.0rem',
    l: '2.8rem'
  }
};

const spacing = {
  xxs: '0.2rem',
  xs: '0.5rem',
  s: '1.0rem',
  m: '1.4rem',
  l: '1.8rem',
  xl: '2.4rem',
  xxl: '3.6rem'
};

const common = {
  button: {
    backgroundColor: color.secondary,
    border: 0,
    borderRadius: '2px',
    color: color.light,
    font: 'inherit',
    padding: spacing.s
  },
  card: {
    backgroundColor: color.primary,
    display: 'flex',
    flexDirection: 'column',
    padding: spacing.s,

    '& h1': {
      letterSpacing: '0.1rem',
      margin: `0 0 ${spacing.s} 0`
    }
  },
  fa_button: {
    backgroundColor: 'transparent',
    border: 0,
    color: color.light,
    font: 'inherit'
  },
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    margin: '0 auto',
    maxWidth: '40rem',
    padding: spacing.xs,
    width: '100%'
  }
};

export default {
  color,
  common,
  font,
  spacing
};
