/* @flow */
import '@babel/polyfill';

// Libraries
import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'react-jss';
import { keys } from 'libp2p-crypto';
import { promisify } from 'util';

// Styles
import theme from './theme';

// Components
import App from './components/App';

// Setup App jsx
const jsx = (
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>
);

// Setup App container
const container = document.getElementById('app');

// Render App
if (container !== null) {
  ReactDOM.render(jsx, container);
}

// import aesjs from 'aes-js';
// import sha256 from 'js-sha256';
// // Generate keypair and store in localStorage
// keys.generateKeyPair('secp256k1', 256, (err, key) => {
//   if (err) {
//     console.error(err);
//     return;
//   }

//   console.log('Original key', key);

//   // Serialize private key
//   const serialized = keys.marshalPrivateKey(key);
//   console.log('Private key', serialized);

//   // Derive AES key
//   const password = new Buffer(sha256('0re0maple').substring(0, 32));
//   console.log('Password', password);

//   // Encrypt private key
//   let aes = new aesjs.ModeOfOperation.ctr(password);
//   const cipherbytes = aes.encrypt(serialized);
//   const cipherhex = aesjs.utils.hex.fromBytes(cipherbytes);
//   console.log('Ciphertext', cipherhex);

//   // Decrypt private key
//   aes = new aesjs.ModeOfOperation.ctr(password);
//   const decodedCipherBytes = aesjs.utils.hex.toBytes(cipherhex);
//   const clearbytes = aes.decrypt(decodedCipherBytes);

//   // Deserialize private key
//   keys.unmarshalPrivateKey(new Buffer(clearbytes, 'hex'), (err, key) => {
//     if (err) console.error(err);
//     console.log('Decrypted key', key);
//   });
// });
