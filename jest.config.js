module.exports = {
  collectCoverageFrom: [
    '<rootDir>/src/**/*.js',
    '!<rootDir>/src/app.js',
    '!<rootDir>/src/theme/*',
    '!<rootDir>/src/**/style.js',
    '!<rootDir>/src/**/*.test.js'
  ],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },
  setupFiles: ['<rootDir>/jest.setup.js'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  testMatch: ['<rootDir>/src/**/*.test.js']
};
